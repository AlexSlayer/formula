package com.formula.arithmetic;

import com.formula.arithmetic.numericsystem.ArabicNumericSystem;
import com.formula.arithmetic.numericsystem.HebrewNumericSystem;
import com.formula.arithmetic.numericsystem.NumericSystem;
import com.formula.arithmetic.numericsystem.RomanNumericSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    private List<NumericSystem> numericSystems;

    public Calculator() {
        numericSystems = new ArrayList<>();

        numericSystems.add(new ArabicNumericSystem());
        numericSystems.add(new RomanNumericSystem());
        numericSystems.add(new HebrewNumericSystem());
    }

    public static void main(String[] args) throws Exception {
        Calculator calculator = new Calculator();

        for (String formula : args) {
            try {
                System.out.println(formula + " = " + calculator.evaluate(formula));
            } catch(Exception e) {
                System.out.println(formula + " : " + e.getMessage());
            }
        }
    }

    public Double evaluate(String formula) {
        if (formula == null || formula.trim().isEmpty()) {
            throw new IllegalArgumentException("Empty formula");
        }

        formula = formula.replaceAll(" ", "").toUpperCase();
        if (formula.startsWith("+") || formula.startsWith("-")) {
            formula = "0" + formula;
        }

        List<String> postfixTokens = toPostfix(formula);

        Stack<Double> operands = new Stack<>();
        try {
            for (String token : postfixTokens) {
                switch (token) {
                    case "+":
                    case "-":
                    case "*":
                    case "/":
                        Double secondOperand = operands.pop();
                        Double firstOperand = operands.pop();

                        operands.push(calculate(firstOperand, secondOperand, token));
                        break;
                    default:
                        operands.push(getOperand(token));
                        break;
                }
            }
        } catch(IllegalArgumentException e) {
            throw e;
        } catch(Exception e) {
            throw new IllegalArgumentException("Incorrect formula");
        }

        Double result = operands.pop();
        if (! operands.isEmpty()) {
            throw new IllegalArgumentException("Incorrect formula");
        }

        return result;
    }

    private List<String> toPostfix(String formula) {
        List<String> postfixTokens = new ArrayList<>();

        Stack<String> operators = new Stack<>();

        StringTokenizer tokenizer = new StringTokenizer(formula, "+-*/", true);
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            switch (token) {
                case "+":
                case "-":
                case "*":
                case "/":
                    while (! operators.isEmpty() && priority(token) <= priority(operators.peek())) {
                        postfixTokens.add(operators.pop());
                    }
                    operators.push(token);
                    break;
                default:
                    postfixTokens.add(token);
                    break;
            }
        }

        while (! operators.isEmpty()) {
            postfixTokens.add(operators.pop());
        }

        return postfixTokens;
    }

    private int priority(String operator) {
        if (operator.equals("/") || operator.equals("*")) {
            return 2;
        } else if (operator.equals("+") || operator.equals("-")) {
            return 1;
        }
        throw new IllegalArgumentException("Wrong operator");
    }

    private Double getOperand(String token) {
        for (NumericSystem numericSystem : numericSystems) {
            Pattern p = Pattern.compile(numericSystem.getPattern());
            Matcher m = p.matcher(token);
            if (m.matches()) {
                return numericSystem.toDouble(token);
            }
        }

        throw new IllegalArgumentException("Unknown numeric system");
    }

    private Double calculate(Double firstOperand, Double secondOperand, String operator) {
        if (operator.equals("+")) {
            return firstOperand + secondOperand;
        } else if (operator.equals("-")) {
            return firstOperand - secondOperand;
        } else if (operator.equals("*")) {
            return firstOperand * secondOperand;
        } else if (operator.equals("/")) {
            return firstOperand / secondOperand;
        }

        throw new IllegalArgumentException("Wrong operator");
    }

}
