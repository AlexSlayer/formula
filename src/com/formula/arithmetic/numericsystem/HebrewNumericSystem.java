package com.formula.arithmetic.numericsystem;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

public class HebrewNumericSystem implements NumericSystem {

    private static final Map<Character, Integer> arabicEquivalents = Collections.unmodifiableMap(
            new Hashtable<Character, Integer>() {{
                put('א', 1);
                put('ב', 2);
                put('ג', 3);
                put('ד', 4);
                put('ה', 5);
                put('ו', 6);
                put('ז', 7);
                put('ח', 8);
                put('ט', 9);
                put('י', 10);
                put('כ', 20);
                put('ל', 30);
                put('מ', 40);
                put('נ', 50);
                put('ס', 60);
                put('ע', 70);
                put('פ', 80);
                put('צ', 90);
                put('ק', 100);
                put('ר', 200);
                put('ש', 300);
                put('ת', 400);
                put('ך', 500);
                put('ם', 600);
                put('ן', 700);
                put('ף', 800);
                put('ץ', 900);
            }}
    );
    private static final char GERESH = '׳';

    @Override
    public String getPattern() {
        String characters = "^[";
        for (Character ch : arabicEquivalents.keySet()) {
            characters += ch;
        }
        characters += GERESH;
        characters += "]+$";

        return characters;
    }

    @Override
    public double toDouble(String source) {
        source = source.replaceAll(" ", "");

        double result = 0;

        for (int i = 0; i < source.length(); i++) {
            char ch = source.charAt(i);

            if (ch == GERESH) continue;

            Integer current = arabicEquivalents.get(ch);
            if (current == null) {
                throw new NumberFormatException("Wrong Hebrew number");
            }

            if (i < source.length() - 1 && source.charAt(i + 1) == GERESH) {
                result = (result == 0) ? current * 1000 : result * current * 1000;
            } else {
                result += current;
            }
        }

        return result;
    }
}
