package com.formula.arithmetic.numericsystem;

public class ArabicNumericSystem implements NumericSystem {

    @Override
    public String getPattern() {
        return "^\\d+(\\.\\d+)?$";
    }

    @Override
    public double toDouble(String source) {
        return Double.parseDouble(source);
    }
}
