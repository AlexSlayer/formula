package com.formula.arithmetic.numericsystem;

/**
 * Interface describes numeric system that can be used in calculations.
 *
 * @see com.formula.arithmetic.Calculator
 */

public interface NumericSystem {
    /**
     * Returns number pattern (Java regular expression)
     *
     * @return number pattern
     */
    public String getPattern();

    /**
     * Returns the {@code double} value of current system number
     *
     * @param source current system number
     * @return the {@code double} value of current system number
     */
    public double toDouble(String source);
}
