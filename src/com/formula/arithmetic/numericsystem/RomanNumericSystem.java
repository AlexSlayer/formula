package com.formula.arithmetic.numericsystem;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

public class RomanNumericSystem implements NumericSystem {

    private static final Map<Character, Integer> arabicEquivalents = Collections.unmodifiableMap(
        new Hashtable<Character, Integer>() {{
            put('I', 1);
            put('V', 5);
            put('X', 10);
            put('L', 50);
            put('C', 100);
            put('D', 500);
            put('M', 1000);
        }}
    );

    @Override
    public String getPattern() {
        String characters = "^[";
        for (Character ch : arabicEquivalents.keySet()) {
            characters += ch;
        }
        characters += "]+$";

        return characters;
    }

    @Override
    public double toDouble(String source) {
        double result = 0;

        for (int i = 0; i < source.length(); i++) {
            Integer current = arabicEquivalents.get(source.charAt(i));
            if (current == null) {
                throw new NumberFormatException("Wrong Roman number");
            }

            if (i < source.length() - 1) {
                Integer next = arabicEquivalents.get(source.charAt(i + 1));
                if (next == null) {
                    throw new NumberFormatException("Wrong Roman number");
                }
                if (next > current) {
                    current = -current;
                }
            }

            result += current;
        }

        if (result > 3999) {
            throw new NumberFormatException("Wrong Roman number");
        }

        return result;
    }
}
