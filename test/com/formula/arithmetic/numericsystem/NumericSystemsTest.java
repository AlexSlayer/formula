package com.formula.arithmetic.numericsystem;

import junit.framework.TestCase;

public class NumericSystemsTest extends TestCase {

    public void testRoman() {
        RomanNumericSystem rns = new RomanNumericSystem();

        assertEquals(19.0, rns.toDouble("XIX"));
        assertEquals(28.0, rns.toDouble("XXVIII"));
        assertEquals(49.0, rns.toDouble("XLIX"));
        assertEquals(890.0, rns.toDouble("DCCCXC"));
        assertEquals(1800.0, rns.toDouble("MDCCC"));
    }

    public void testHebrew() {
        HebrewNumericSystem hns = new HebrewNumericSystem();

        assertEquals(1000.0, hns.toDouble("א׳"));
        assertEquals(1000000.0, hns.toDouble("א׳ א׳"));
        assertEquals(3000764.0, hns.toDouble("ג׳ א׳ תשסד"));
        assertEquals(5821.0, hns.toDouble("ה׳ תתכא"));
        assertEquals(715.0, hns.toDouble("תשטו"));
    }

}
