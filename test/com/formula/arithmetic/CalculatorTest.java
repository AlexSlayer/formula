package com.formula.arithmetic;

import junit.framework.TestCase;

public class CalculatorTest extends TestCase {

    public void testCalculator() {
        Calculator calculator = new Calculator();

        assertEquals(123.0, calculator.evaluate("123+CXXIII-13-CX"));
        assertEquals(-978.0, calculator.evaluate("8*2 - III + IV + 10/2 - א׳"));
        assertEquals(-4.0, calculator.evaluate("-10 / 2 + 4 * 2.5 - IX"));
        assertEquals(95.0, calculator.evaluate("50/5/2 + 10*3*3"));

        boolean exThrown = false;
        try {
            calculator.evaluate("8 * ABCDE + 16");
        } catch(Exception e) {
            exThrown = true;
        }
        assertTrue(exThrown);

        exThrown = false;
        try {
            calculator.evaluate("/2 - 8 + III");
        } catch(Exception e) {
            exThrown = true;
        }
        assertTrue(exThrown);
    }

}
